require "capistrano"
require "capistrano/current_branch/version"

module Capistrano::CurrentBranch
  def self.load_into(configuration)
    configuration.load do
      def current_branch
        `git symbolic-ref --short HEAD`.chomp
      end
    end
  end
end

if Capistrano::Configuration.instance
  Capistrano::CurrentBranch.load_into(Capistrano::Configuration.instance)
end
