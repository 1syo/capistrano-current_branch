# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'capistrano/current_branch/version'

Gem::Specification.new do |spec|
  spec.name          = "capistrano-current_branch"
  spec.version       = Capistrano::CurrentBranch::VERSION
  spec.authors       = ["TAKAHASHI Kazunari"]
  spec.email         = ["takahashi@1syo.net"]
  spec.description   = %q{Automatically select git remote repository in capistrano recipes.}
  spec.summary       = %q{Automatically select git remote repository in capistrano recipes.}
  spec.homepage      = "https://github.com/1syo/capistrano-current_branch"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "capistrano"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "capistrano-spec"
end
