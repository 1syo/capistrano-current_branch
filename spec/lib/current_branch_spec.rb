require 'spec_helper'

describe Capistrano::CurrentBranch do
  include_context "capistrano"

  describe "#current_branch" do
    before do
      Capistrano::CurrentBranch.load_into(subject)
      Object.any_instance.stub(:`).with("git symbolic-ref --short HEAD") { "master" }
    end

    its(:current_branch) { should eq "master" }
  end
end
